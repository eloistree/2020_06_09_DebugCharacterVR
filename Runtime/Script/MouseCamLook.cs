
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseCamLook : MonoBehaviour
{

    [SerializeField] float m_sensitivity = 5.0f;
    [SerializeField] float m_smoothing = 2.0f;
    [SerializeField] Transform m_horiontalRoot;
    [SerializeField] Transform m_verticalHead;
    [Header("Debug")]
    [SerializeField] Vector2 m_mouseLook;
    private Vector2 m_smooth;
    public float m_neckVerticalAngle = 20;
    public float m_backSkullVerticalAngle = 110;

    void Update()
    {

        Vector2 md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        md = Vector2.Scale(md, new Vector2(m_sensitivity * m_smoothing, m_sensitivity * m_smoothing));
        m_smooth.x = Mathf.Lerp(m_smooth.x, md.x, 1f / m_smoothing);
        m_smooth.y = Mathf.Lerp(m_smooth.y, md.y, 1f / m_smoothing);
        m_mouseLook += m_smooth;

        m_mouseLook.y=Mathf.Clamp(m_mouseLook.y, -(90-m_neckVerticalAngle), m_backSkullVerticalAngle);
        if (m_mouseLook.x > 180f) m_mouseLook.x = -180f;
        if (m_mouseLook.x <-180f) m_mouseLook.x = 180;

        m_verticalHead.localRotation = Quaternion.AngleAxis(-m_mouseLook.y, Vector3.right);
        m_horiontalRoot.localRotation = Quaternion.Euler(0, m_mouseLook.x, 0);

        // vector3.right means the x-axis
        //transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        //character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
    }

    private void Reset()
    {
        m_verticalHead = transform;
        m_horiontalRoot = transform.parent ;
    }
}